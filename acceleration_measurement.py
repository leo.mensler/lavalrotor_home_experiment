import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#path_setup_json = "datasheets/setup_bohrer.json"
path_setup_json = "datasheets/setup_phone.json" # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

sensor_uuid = "1ee847be-fddd-6ee4-892a-68c4555b0981"

Data_dict = {
    sensor_uuid: {
        "acceleration_x": [],
        "acceleration_y": [],
        "acceleration_z": [],
        "timestamp": [],
    }
}




# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
start_time = time.time()
passed_time = 0

while passed_time < measure_duration_in_s:

    Data_dict[sensor_uuid]["acceleration_x"].append(accelerometer.acceleration[0])
    Data_dict[sensor_uuid]["acceleration_y"].append(accelerometer.acceleration[1])
    Data_dict[sensor_uuid]["acceleration_z"].append(accelerometer.acceleration[2])
    Data_dict[sensor_uuid]["timestamp"].append(passed_time)
    
    time.sleep(0.001)
    passed_time = time.time()-start_time
    


# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
with h5py.File(path_h5_file, 'w') as f:
    group = f.create_group("RawData")
    
    daten_x = group.create_dataset("acceleration_x", data=Data_dict[sensor_uuid]["acceleration_x"])
    daten_y = group.create_dataset("acceleration_y", data=Data_dict[sensor_uuid]["acceleration_y"])
    daten_z = group.create_dataset("acceleration_z", data=Data_dict[sensor_uuid]["acceleration_z"])
    daten_time = group.create_dataset("timestamp", data=Data_dict[sensor_uuid]["timestamp"])
    
    
    daten_x.attrs['units'] = "m/s^2"
    daten_y.attrs['units'] = "m/s^2"
    daten_z.attrs['units'] = "m/s^2"
    daten_time.attrs['units'] = "s"
    
    
    
    

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))

"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    
    length = len(x)
    a_abs = []
    
    #berechnen der euklidischen norm des beschleunigungsvektors
    for i in range(length):
        a = (x[i]**2+y[i]**2+z[i]**2)**(1/2)
        a_abs.append(a)
        
    return a_abs
    
    
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    
    #interpolieren der Zeitdaten mithilfe der linspace methode
    new_time = np.linspace(time.min(), time.max(), num=len(time), endpoint = True)
    #interpolieren der Daten mit der "interp-methode"
    new_data = np.interp(new_time, time, data)
    
    return (new_time, new_data)

    """
    Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    pass

from numpy.fft import fft
import numpy as np
from typing import Tuple

def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    
    #erstellen der my-fft-funktion mithilfe des in der aufgabenstellung verlinkten Schemas.
    X = fft(x)
    N = len(X)

    # Berechnung der Abtastrate aus Zeitdaten
    sr = 1 / np.mean(np.diff(time))
    
    # Erzeugung des Frequenzarrays
    freq = np.fft.fftfreq(N, 1/sr)
    half = N // 2
    X = X[:half]
    amplitude = np.abs(X)
    
    half = N // 2
    
    #wichtig ist auch hier, nur die hälfte des frequenzarrays zurückzugeben. (Man will nur das
    #positive Frequenzspektrum betrachten).Bei "amplitude" ist das bereits in zeile 72 geschehen.
    
    return amplitude, freq[:half]

    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
